package com.gareth.buckbeak.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by gareth on 12/24/17.
 */

@DatabaseTable(tableName = "users")
public class User implements Serializable {
    @DatabaseField(columnName = "user_id",generatedId = true)
    private long id;

    @DatabaseField
    private String username;

    @DatabaseField
    private String password;

    @DatabaseField
    private String sex;

    @DatabaseField
    private Integer age;

    @DatabaseField
    private Integer yearsOfExperience;

    @DatabaseField
    private String nameOfSchool;

    @DatabaseField
    private String symptoms;

    @DatabaseField
    private String habbits;



    public User() {}

    public User(String username, String password, String sex, Integer age, Integer yearsOfExperience, String nameOfSchool, String symptoms, String habbits) {
        this.username = username;
        this.password = password;
        this.sex = sex;
        this.age = age;
        this.yearsOfExperience = yearsOfExperience;
        this.nameOfSchool = nameOfSchool;
        this.symptoms = symptoms;
        this.habbits = habbits;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(Integer yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getNameOfSchool() {
        return nameOfSchool;
    }

    public void setNameOfSchool(String nameOfSchool) {
        this.nameOfSchool = nameOfSchool;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getHabbits() {
        return habbits;
    }

    public void setHabbits(String habbits) {
        this.habbits = habbits;
    }
}
