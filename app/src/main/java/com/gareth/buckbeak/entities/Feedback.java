package com.gareth.buckbeak.entities;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by gareth on 12/25/17.
 */

public class Feedback implements Serializable {

    @DatabaseField(columnName = "feedback_id",generatedId = true)
    private long id;

    private String username;

    private String dateTime;

    private String feedback;

    public Feedback() {}

    public Feedback(String username, String dateTime, String feedback) {
        this.username = username;
        this.dateTime = dateTime;
        this.feedback = feedback;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
