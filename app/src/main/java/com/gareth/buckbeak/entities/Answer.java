package com.gareth.buckbeak.entities;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;



public class Answer implements Serializable {

    private final String ID="answer_id";
    private final String TEXT="answer";
    private final String IS_TRUE="is_true";


    @DatabaseField(columnName = ID,generatedId = true)
    private long id;

    @DatabaseField(columnName = ID,generatedId = true)
    private String text;

    @DatabaseField(columnName = ID,generatedId = true)
    private boolean isTrue;



    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }
}
