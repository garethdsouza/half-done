package com.gareth.buckbeak.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gareth.buckbeak.R;

import static com.gareth.buckbeak.Common.USERNAME;
import static com.gareth.buckbeak.Common.getTipOfTheDay;

public class MenuActivity extends AppCompatActivity{

    private Button tipOfTheDay,reminder,questionnaire,logout;
    private SharedPreferences sharedpreferences;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        tipOfTheDay = findViewById(R.id.btnTip);
        reminder = findViewById(R.id.btnReminder);
        questionnaire = findViewById(R.id.btnQuestionnaire);
        logout = findViewById(R.id.btnLogout);

        tipOfTheDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(MenuActivity.this).create(); //Read Update
                alertDialog.setTitle("Tip for the day");
                alertDialog.setMessage(getTipOfTheDay());
                alertDialog.show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SharedPreferences.Editor editor = sharedpreferences.edit();
                //editor.remove(USERNAME);
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ReminderActivity.class));
            }
        });

        questionnaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),QuesntionActivity.class));
            }
        });
    }
}
