package com.gareth.buckbeak.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gareth.buckbeak.R;

import java.util.ArrayList;
import java.util.List;

import static com.gareth.buckbeak.Common.getQuestions;

public class QuesntionActivity extends Activity {
    private ListView myList;
    private MyAdapter myAdapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quesntion);

        myList = (ListView) findViewById(R.id.MyList);
        myList.setItemsCanFocus(true);

        final ArrayList<String> myItems = new ArrayList<>();
        for (int i = 0; i < getQuestions().size(); i++) {
            myItems.add("Caption" + i);
        }
        myAdapter = new MyAdapter(myItems,getQuestions());
        myList.setAdapter(myAdapter);

        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (String i:myItems) {
                    System.out.println(i.toString());
                }
            }
        });



    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<String> answers;
        public List<String> questions;



        public MyAdapter(ArrayList answers, List<String> questions) {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.answers =answers;
            this.questions=questions;
            notifyDataSetChanged();
        }



        public int getCount() {
            return answers.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.question_item, null);
                holder.question= convertView.findViewById(R.id.tvQuestion);
                holder.answer = (EditText) convertView.findViewById(R.id.etAnswer);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            //Fill EditText with the value you have in data source
            holder.question.setId(position);
            holder.question.setText(questions.get(position));
            holder.answer.setText(answers.get(position));
            holder.answer.setId(position);
            holder.answer.requestFocus();
            //we need to update adapter once we finish with editing
            holder.answer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus){
                        final int position = v.getId();
                        final EditText answer = (EditText) v;
                        answers.set(position,answer.getText().toString());

                    }
                }
            });

            return convertView;
        }
    }



    class ViewHolder {
        EditText answer;
        TextView question;
    }

}
