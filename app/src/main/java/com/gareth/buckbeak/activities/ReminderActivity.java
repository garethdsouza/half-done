package com.gareth.buckbeak.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gareth.buckbeak.AlarmReceiver;
import com.gareth.buckbeak.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.gareth.buckbeak.Common.MyPREFERENCES;
import static com.gareth.buckbeak.Common.REMINDERS;
import static com.gareth.buckbeak.Common.getFormattedTime;

public class ReminderActivity extends AppCompatActivity {

    private Button add;
    private int mHour, mMinute;
    private ListView listView;
    private List<String> reminders;
    private SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        listView =findViewById(R.id.lvReminder);

        reminders= getReminders();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.reminder_item, R.id.tv_alarm, reminders);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ReminderActivity.this,reminders.get(i),Toast.LENGTH_SHORT).show();
                removeReminderPreference(reminders.get(i));
                reminders.remove(i);
                adapter.notifyDataSetChanged();

            }
        });

        add = findViewById(R.id.btnAdd);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                if(reminders.size() > 7){
                    Toast.makeText(ReminderActivity.this,"You cannot add more than 7 reminders!",Toast.LENGTH_LONG).show();
                }
                else{
                    TimePickerDialog timePickerDialog = new TimePickerDialog(ReminderActivity.this,new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            Toast.makeText(getApplicationContext(),hourOfDay + ":" + minute,Toast.LENGTH_LONG).show();
                            reminders.add(getFormattedTime(hourOfDay + ":" + minute));
                            addReminderPreferences(hourOfDay + ":" + minute);
                            adapter.notifyDataSetChanged();
                            createAlarmForNotification(hourOfDay,minute,reminders.size());

                        }
                    }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
            }
        });

    }



    private void removeReminderPreference(String i) {
        Set<String> reminderSet = sharedpreferences.getStringSet(REMINDERS, new HashSet<String>());
        i = i.replace("AM","");
        i = i.replace("PM","");
        i = i.replace(" ","");
        reminderSet.remove(i);
        SharedPreferences.Editor editor =sharedpreferences.edit();
        editor.clear();
        editor.putStringSet(REMINDERS,reminderSet);
        editor.apply();

        reminderSet = sharedpreferences.getStringSet(REMINDERS, new HashSet<String>());
        System.out.println(reminderSet);
    }

    private void addReminderPreferences(String s) {
        Set<String> reminderSet = sharedpreferences.getStringSet(REMINDERS, new HashSet<String>());
        reminderSet.add(s);
        SharedPreferences.Editor editor =sharedpreferences.edit();
        editor.clear();
        editor.putStringSet(REMINDERS,reminderSet);
        editor.apply();
    }

    public List<String> getReminders() {
        Set<String> reminderSet = sharedpreferences.getStringSet(REMINDERS, new HashSet<String>());
        List<String> formattedTime = new ArrayList<>();
        for (String s:reminderSet) {
            formattedTime.add(getFormattedTime(s));
        }
        return formattedTime;
    }

    private void createAlarmForNotification(Integer hour,Integer min,Integer requestCode){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        Intent intent1 = new Intent(ReminderActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ReminderActivity.this, requestCode,intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) ReminderActivity.this.getSystemService(ReminderActivity.this.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    private void removeNotification(Integer requestCode){
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ReminderActivity.this, requestCode,new Intent(ReminderActivity.this, AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) ReminderActivity.this.getSystemService(ReminderActivity.this.ALARM_SERVICE);
        am.cancel(pendingIntent);
    }
}
