package com.gareth.buckbeak.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gareth.buckbeak.R;
import com.gareth.buckbeak.db.DatabaseHelper;
import com.gareth.buckbeak.entities.User;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.CloseableIterable;

import java.sql.SQLException;
import java.util.List;

public class NewUserActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;
    private EditText username,password;
    private TextView errorMessage;
    private Button save;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        save = findViewById(R.id.btnSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uname = username.getText().toString().trim();
                String passw = password.getText().toString().trim();
                if(uname == null || passw == null || uname.isEmpty() || passw.isEmpty()){
                    errorMessage.setText("Username / Password cannot be empty");
                    errorMessage.setVisibility(View.VISIBLE);
                }
                else if(doesUserExist(uname)){
                    errorMessage.setText("Username already exists");
                    errorMessage.setVisibility(View.VISIBLE);
                }
                else{
                    if(errorMessage.getVisibility() == View.VISIBLE){
                        errorMessage.setVisibility(View.GONE);
                    }
                    User user = new User(uname,passw);
                    try {
                        getDatabaseHelper().getUserDao().create(user);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private boolean doesUserExist(String username) {
        try {
            List<User> userList = getDatabaseHelper().getUserDao().queryBuilder().where().eq("username",username).query();
            if(userList != null || !userList.isEmpty()){
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if(databaseHelper == null){
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(),DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
