package com.gareth.buckbeak.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gareth.buckbeak.R;
import com.gareth.buckbeak.db.DatabaseHelper;
import com.gareth.buckbeak.entities.User;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.List;

import static com.gareth.buckbeak.Common.ADMIN;
import static com.gareth.buckbeak.Common.MyPREFERENCES;
import static com.gareth.buckbeak.Common.USERNAME;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;
    private EditText username,password;
    private Button login,createUser;
    private SharedPreferences sharedpreferences;
    private TextView errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        username = (EditText) findViewById(R.id.et_username);
        password = (EditText) findViewById(R.id.et_password);
        login = findViewById(R.id.btnLogin);
        createUser = findViewById(R.id.btnNewUser);
        errorMessage = findViewById(R.id.tv_error);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uname = username.getText().toString().trim();
                String passw = password.getText().toString().trim();

                if(uname == null || passw == null || uname.isEmpty() || passw.isEmpty()){
                    errorMessage.setText("Username / Password cannot be empty");
                    errorMessage.setVisibility(View.VISIBLE);
                }
                else if(!doesUserExist(uname,passw)){
                    errorMessage.setText("Username and password do not match");
                    errorMessage.setVisibility(View.VISIBLE);
                }
                else{
                    if(errorMessage.getVisibility() == View.VISIBLE){
                        errorMessage.setVisibility(View.GONE);
                    }
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(USERNAME,username.getText().toString());
                    startActivity(new Intent(getApplicationContext(),MenuActivity.class));
                }
            }

            });

        createUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),NewUserActivity.class));
            }
        });
    }

    private boolean isAdmin(String uname, String passw) {
        if(ADMIN.equals(uname) && ADMIN.equals(passw)){
            return true;
        }else{
            return false;
        }
    }

    private boolean doesUserExist(String username,String password) {
        if(ADMIN.equals(username)){
            return isAdmin(username,password);
        }

        try {
            List<User> userList = getDatabaseHelper().getUserDao().queryBuilder().where().eq("username",username).and().eq("password",password).query();
            if(userList != null || !userList.isEmpty()){
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if(databaseHelper == null){
            databaseHelper = OpenHelperManager.getHelper(getApplicationContext(),DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
