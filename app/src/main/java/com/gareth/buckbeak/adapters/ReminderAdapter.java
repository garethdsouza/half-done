package com.gareth.buckbeak.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gareth.buckbeak.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gareth on 12/25/17.
 */

public class ReminderAdapter  extends ArrayAdapter<String> implements View.OnClickListener{

    private List<String> dataSet;
    Context mContext;

    public ReminderAdapter(@NonNull Context context, @NonNull List<String> objects) {
        super(context, R.layout.reminder_item, objects);
        this.dataSet = objects;
        this.mContext = context;

    }


    @Override
    public void onClick(View view) {

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.reminder_item, parent, false);
        }
        convertView.findViewById(R.id.tv_alarm);
        return convertView;
    }
}
