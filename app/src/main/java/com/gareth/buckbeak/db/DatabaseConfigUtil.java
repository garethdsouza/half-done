package com.gareth.buckbeak.db;


import com.gareth.buckbeak.entities.User;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by gareth on 26/07/17.
 */

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final String ORMLITE_CONFIGURATION_FILE_NAME = "ormlite_config.txt";


    private static final Class<?>[] classes = new Class[] {
            User.class
    };


    public static void main(String[] args) throws SQLException, IOException {
        File configFile = new File(new File("").getAbsolutePath()
                .split("app" +File.separator + "build")[0] + File.separator +
                "app" + File.separator +
                "src" + File.separator +
                "main" + File.separator +
                "res" + File.separator +
                "raw" + File.separator +
                ORMLITE_CONFIGURATION_FILE_NAME);
        writeConfigFile(configFile,classes);
    }
}