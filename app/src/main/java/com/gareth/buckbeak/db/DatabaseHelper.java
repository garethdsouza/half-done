package com.gareth.buckbeak.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gareth.buckbeak.R;
import com.gareth.buckbeak.entities.Feedback;
import com.gareth.buckbeak.entities.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by gareth on 25/07/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "shoppingApp.db";

    private static final int DATABASE_VERSION = 1;

    //Dao Lists

    private Dao<User, Long> userDao = null;
    private RuntimeExceptionDao<User, Long> userRuntimeDao = null;
    private Dao<Feedback, Long> feedbackDao = null;
    private RuntimeExceptionDao<Feedback, Long> feedbackRuntimeDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }



    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            TableUtils.createTable(connectionSource, Feedback.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public Dao<User, Long> getUserDao() throws SQLException {
        if(userDao == null){
            userDao = getDao(User.class);
        }
        return userDao;
    }

    public RuntimeExceptionDao<User, Long> getUserRuntimeDao() {
        if(userRuntimeDao == null){
            userRuntimeDao = getRuntimeExceptionDao(User.class);
        }
        return userRuntimeDao;
    }


    public Dao<Feedback, Long> getFeedbackDao() throws SQLException {
        if(feedbackDao == null){
            feedbackDao = getDao(Feedback.class);
        }
        return feedbackDao;
    }

    public RuntimeExceptionDao<Feedback, Long> getFeedbackRuntimeDao() {
        if(feedbackRuntimeDao == null){
            feedbackRuntimeDao = getRuntimeExceptionDao(Feedback.class);
        }
        return feedbackRuntimeDao;
    }

    public void clearTables(Object object){
        try {
            TableUtils.clearTable(getConnectionSource(),object.getClass());
            Log.d(TAG, "Cleared Table :" + object.getClass().getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
