package com.gareth.buckbeak;

import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.SimpleFormatter;

/**
 * Created by gareth on 12/25/17.
 */

public class Common {

    public static final String USERNAME = "USERNAME";
    public static final String ADMIN = "admin";
    public static final String REMINDERS = "REMINDERS";
    public static final String SEPERATOR = "|;|";
    public static final String MyPREFERENCES = "AppPrefences";

    public static List<String> convertStringToList(String example){
        String[] brokenString = example.split(SEPERATOR);
        return Arrays.asList(brokenString);
    }

    public static String convertListToString(List<String> example){
        String s = "";
        for (String e:example) {
            s += e+SEPERATOR;
        }
        return s;
    }

    public static String getFormattedDateTimeForFeedBack(){
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        return formatter.format(Calendar.getInstance().getTime());
    }

    public static String getFormattedTime(String time){
        String[] stime = time.split(":");
        Integer hour = Integer.parseInt(stime[0]);
        Integer min = Integer.parseInt(stime[1]);
        String formattedTime = hour%12+" : "+min+" ";
        if(hour >11){
            formattedTime +="PM";
        }else{
            formattedTime +="AM";
        }
        return formattedTime;
    }

    public static String getTipOfTheDay(){
        return "This is an awesome tip!";
    }


    public static List<String> getQuestions(){
        String[] questions ={"Question1","Question2","Question3","Question1","Question2","Question3"};
        return Arrays.asList(questions);
    }
}
